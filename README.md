# Crafting interpreters - lox lang

https://craftinginterpreters.com/

Just going through this bit by bit, but rather than doing it in Java and then C,
this is all done in Rust.

Should be fun :)

--

Start the interpreter in REPL mode by running without args, or pass it a file in
the first arg.

```
Usage: lox [<input_file>]

Rusty lox interpreter

Positional Arguments:
  input_file        file to interpret

Options:
  --help            display usage information
```
