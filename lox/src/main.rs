use argh::FromArgs;
use std::io::{self, Write};
use std::path::{Path, PathBuf};
use std::{fs, process};

pub mod errors;
mod scanner;

#[derive(FromArgs)]
/// Rusty lox interpreter
struct LoxArgs {
    /// file to interpret
    #[argh(positional)]
    input_file: Option<PathBuf>,
}

fn main() {
    let args: LoxArgs = argh::from_env();
    process::exit(match args.input_file {
        Some(input_file) => file_mode(&input_file),
        None => repl_mode(),
    })
}

fn file_mode(path: &Path) -> i32 {
    match fs::read_to_string(path) {
        Err(e) => {
            println!("trouble reading file {e}");
            1
        }
        Ok(contents) => scanner::scan(&contents) as i32,
    }
}

fn repl_mode() -> i32 {
    println!("Lox REPL v0.1 - q, quit, or ^C to exit");
    let mut input = "".to_owned();
    while input != "q" && input != "quit" {
        print!("> ");
        io::stdout().flush().expect("Could not flush stdout");
        io::stdin()
            .read_line(&mut input)
            .expect("Failed to read input");
        input = input.trim().to_owned();
        scanner::scan(&input);
    }
    0
}
