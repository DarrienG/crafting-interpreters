use thiserror::Error;

#[derive(Error, Debug)]
pub enum ParseError {
    #[error("{line_number:?} | {line:?}\nSyntax error {error:?}")]
    SyntaxError {
        line_number: usize,
        line: String,
        error: String,
    },
}
