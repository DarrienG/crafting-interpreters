use crate::errors::ParseError;
use crate::scanner::Token::EOF;

pub fn scan(input: &str) -> usize {
    match input_to_tokens(input) {
        Ok(tokens) => tokens.iter().for_each(|token| println!("{:?}", token)),
        Err(e) => {
            eprintln!("Got error parsing: {:?}", e);
            return 1;
        }
    }
    0
}

#[derive(Debug)]
enum Token {
    // Single-character tokens.
    // line: usize
    // why store the lexeme we know what it is
    LeftParen(usize),
    RightParen(usize),
    LeftBrace(usize),
    RightBrace(usize),
    Comma(usize),
    Dot(usize),
    Minus(usize),
    Plus(usize),
    Semicolon(usize),
    Slash(usize),
    Star(usize),

    // One or two character tokens.
    // line: usize
    Bang(usize),
    BangEqual(usize),
    Equal(usize),
    EqualEqual(usize),
    Greater(usize),
    GreaterEqual(usize),
    Less(usize),
    LessEqual(usize),

    // Literals.
    Identifier(String, usize),
    String(String, usize),
    Number(String, usize),

    // Keywords.
    And(usize),
    Class(usize),
    Else(usize),
    False(usize),
    Fun(usize),
    For(usize),
    If(usize),
    Nil(usize),
    Or(usize),
    Print(usize),
    Return(usize),
    Super(usize),
    This(usize),
    True(usize),
    Var(usize),
    While(usize),

    #[warn(clippy::upper_case_acronyms)]
    EOF(String, usize),
}

fn input_to_tokens(input: &str) -> Result<Vec<Token>, ParseError> {
    let mut tokens = vec![];
    let mut line_count: usize = 0;
    for (line_number, line) in input.lines().enumerate() {
        tokens.append(&mut scan_line_to_tokens(line_number, line)?);

        // bookkeeping for the end
        line_count = line_number;
    }
    tokens.push(EOF("".to_owned(), line_count));
    Ok(vec![])
}

fn scan_line_to_tokens(line_number: usize, line: &str) -> Result<Vec<Token>, ParseError> {
    let mut tokens = vec![];
    let line_chars = line.chars().collect::<Vec<char>>();
    let mut idx = 0;
    while idx < line_chars.len() {
        match line_chars[idx] {
            // single characters
            '(' => tokens.push(Token::LeftParen(line_number)),
            ')' => tokens.push(Token::RightParen(line_number)),
            '{' => tokens.push(Token::LeftBrace(line_number)),
            '}' => tokens.push(Token::RightBrace(line_number)),
            ',' => tokens.push(Token::Comma(line_number)),
            '.' => tokens.push(Token::Dot(line_number)),
            '-' => tokens.push(Token::Minus(line_number)),
            '+' => tokens.push(Token::Plus(line_number)),
            ';' => tokens.push(Token::Semicolon(line_number)),
            '*' => tokens.push(Token::Star(line_number)),
            // characters that may be double characters
            '!' => tokens.push(if peek_match('!', idx, &line_chars) {
                idx += 1;
                Token::BangEqual(line_number)
            } else {
                Token::Bang(line_number)
            }),
            '=' => tokens.push(if peek_match('=', idx, &line_chars) {
                idx += 1;
                Token::EqualEqual(line_number)
            } else {
                Token::Equal(line_number)
            }),
            '<' => tokens.push(if peek_match('=', idx, &line_chars) {
                idx += 1;
                Token::LessEqual(line_number)
            } else {
                Token::Less(line_number)
            }),
            '>' => tokens.push(if peek_match('=', idx, &line_chars) {
                idx += 1;
                Token::GreaterEqual(line_number)
            } else {
                Token::Greater(line_number)
            }),
            '/' => {
                if peek_match('/', idx, &line_chars) {
                    // comment goes until the end of the line
                    idx = line_chars.len()
                } else {
                    tokens.push(Token::Slash(idx));
                }
            }
            ' ' | '\r' | '\t' => {
                // ignore whitespace
                idx += 1;
            }
            '"' => {
                let string_res = scan_string(idx, &line_chars, line_number, line)?;
                idx = string_res.1;
                let full_string = string_res.0;
                tokens.push(Token::String(full_string, idx));
            }
            _ => {
                let token = line_chars[idx];
                if token.is_digit(10) {
                    let scan_result = scan_number(idx, &line_chars);
                    tokens.push(Token::Number(scan_result.0, idx));
                    idx = scan_result.1;
                } else if token.is_alphabetic() {
                    let scan_result = scan_identifier(idx, &line_chars);
                    tokens.push(scan_result.0);
                    idx = scan_result.1;
                } else {
                    return Err(ParseError::SyntaxError {
                        line_number,
                        line: line.to_owned(),
                        error: format!("Unexpected character: {}", token),
                    });
                }
            }
        }
        idx += 1;
    }
    Ok(vec![])
}

/// Looks ahead one character and return if the character is the same
fn peek_match(to_match: char, idx: usize, line_chars: &[char]) -> bool {
    let peek_idx = idx + 1;
    if peek_idx > line_chars.len() {
        false
    } else {
        line_chars[idx] == to_match
    }
}

/// Looks ahead one character and returns character if not at the end of the line.
fn peek(idx: usize, line_chars: &[char]) -> Option<char> {
    let peek_idx = idx + 1;
    if peek_idx > line_chars.len() {
        None
    } else {
        Some(line_chars[peek_idx])
    }
}

/// Scan from first quotation mark in string until the last quotation mark in string
/// or return syntax error.
/// Strings may only span one line, so if we get to the end of a line without terminating the
/// string, we know we have a problem.
fn scan_string(
    mut idx: usize,
    line_chars: &[char],
    line_number: usize,
    line: &str,
) -> Result<(String, usize), ParseError> {
    let mut string_value = "".to_owned();
    let mut last_peek = peek(idx, line_chars);
    loop {
        idx += 1;
        match last_peek {
            Some(value) => {
                if value == '"' {
                    return Ok((string_value, idx));
                } else {
                    string_value.push(value);
                    idx += 1;
                    last_peek = peek(idx, line_chars);
                }
            }
            None => {
                return Err(ParseError::SyntaxError {
                    line_number,
                    line: line.to_owned(),
                    error: "Unterminated string".to_owned(),
                })
            }
        }
    }
}

fn scan_number(mut idx: usize, line_chars: &[char]) -> (String, usize) {
    let mut scanned_number = format!("{}", line_chars[idx]);

    loop {
        match peek(idx, line_chars) {
            Some(maybe_next_digit) => {
                if maybe_next_digit.is_digit(10) {
                    scanned_number.push(maybe_next_digit);
                    idx += 1;
                }
                break;
            }
            None => return (scanned_number, idx + 1),
        }
    }

    if peek_match('.', idx, line_chars)
        && matches!(peek(idx + 1, line_chars), Some(num) if num.is_digit(10))
    {
        scanned_number.push(line_chars[idx + 1]);
        scanned_number.push(line_chars[idx + 2]);
        idx += 2;
    }

    // yes it repeats, this the first few statements, this is probably the cleanest way to do it
    // at least for now.
    loop {
        match peek(idx, line_chars) {
            Some(maybe_next_digit) => {
                if maybe_next_digit.is_digit(10) {
                    scanned_number.push(maybe_next_digit);
                    idx += 1;
                }
                break;
            }
            None => return (scanned_number, idx + 1),
        }
    }

    return (scanned_number, idx);
}

fn scan_identifier(mut idx: usize, line_chars: &[char]) -> (Token, usize) {
    let mut identifier = "".to_owned();
    while line_chars[idx].is_alphanumeric() {
        identifier.push(line_chars[idx]);
        idx += 1;
    }
    (
        match identifier.as_ref() {
            "and" => Token::And(idx),
            "class" => Token::Class(idx),
            "else" => Token::Else(idx),
            "false" => Token::False(idx),
            "for" => Token::For(idx),
            "fun" => Token::Fun(idx),
            "if" => Token::If(idx),
            "nil" => Token::Nil(idx),
            "or" => Token::Or(idx),
            "print" => Token::Print(idx),
            "return" => Token::Return(idx),
            "super" => Token::Super(idx),
            "this" => Token::This(idx),
            "true" => Token::True(idx),
            "var" => Token::Var(idx),
            "while" => Token::While(idx),
            _ => Token::Identifier(identifier, idx),
        },
        idx,
    )
}
